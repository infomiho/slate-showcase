import {
  UPDATE_FORM,
  SEND_TO_SERVER,
  SET_LOADING
} from "../actions/updateForm";

const initialState = {
  name: "",
  description: "",
  sentToServer: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_FORM:
      return {
        ...state,
        ...action.payload
      };
    case SEND_TO_SERVER:
      const { name, description } = state;
      return {
        ...state,
        sentToServer: { name, description }
      };
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    default:
      return state;
  }
};
