import React, { Component } from "react";
import ReactQuill from "react-quill";
import { connect } from "react-redux";

import * as updateFormActions from "./actions/updateForm";

import "./App.css";
import "react-quill/dist/quill.snow.css";
import Loader from "./components/Loader";

class App extends Component {
  onSubmit = event => {
    event.preventDefault();
    this.props.sendToServer();
  };

  render() {
    console.log(this.props);
    const { updateForm } = this.props;
    const { name, description, sentToServer, loading } = this.props.form;
    return (
      <div className={`App ${loading ? " App--loading" : ""}`}>
        <header className="AppBody">
          <h1>Using Slate.js component with Redux</h1>
          <p>
            This demo uses{" "}
            <a
              href="https://www.slatejs.org/examples/richtext"
              target="_blank"
              rel="noopener noreferrer"
            >
              Slate.js
            </a>{" "}
            framework for the rich text editing field. It also uses{" "}
            <a
              href="https://redux.js.org/"
              target="_blank"
              rel="noopener noreferrer"
            >
              Redux
            </a>{" "}
            as its data store and simulates sending a request to a backend.
          </p>
          <Loader />
          <div className="AppForm">
            <form onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="name">Name</label>
                <input
                  type="text"
                  id="name"
                  className="form-control"
                  value={name}
                  onChange={event => updateForm("name", event.target.value)}
                />
              </div>
              <div className="form-group">
                <label htmlFor="description">Description</label>
                <ReactQuill
                  value={description}
                  onChange={value => updateForm("description", value)}
                />
              </div>
              <button className="btn btn-block btn-primary">Save</button>
            </form>
            {sentToServer ? (
              <div className="card server">
                <div className="card-body">
                  <h2>What was sent to the server</h2>
                  <pre>
                    <code>{JSON.stringify(sentToServer, null, 4)}</code>
                  </pre>
                </div>
              </div>
            ) : null}
          </div>
        </header>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { ...state };
};
const mapDispatchToProps = {
  ...updateFormActions
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
