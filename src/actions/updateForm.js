export const UPDATE_FORM = "UPDATE_FORM";

export const updateForm = (key, value) => dispatch => {
  dispatch({
    type: UPDATE_FORM,
    payload: { [key]: value }
  });
};

export const SEND_TO_SERVER = "SEND_TO_SERVER";
export const SET_LOADING = "SET_LOADING";

export const sendToServer = () => dispatch => {
  dispatch({
    type: SET_LOADING,
    payload: true
  });
  setTimeout(() => {
    dispatch({
      type: SEND_TO_SERVER
    });
    dispatch({
      type: SET_LOADING,
      payload: false
    });
  }, 2000);
};
